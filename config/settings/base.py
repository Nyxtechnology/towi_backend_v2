import os
import sys
from unipath import Path

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = Path(__file__).ancestor(3)
sys.path.append(BASE_DIR.child('apps'))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '4^7n(u38@eh%uph00wgip67ro*_g8yh7(!kvp$c45rm=p4z*_&'

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'reusable.apps.ReusableConfig',
    'accounts.apps.AccountsConfig',
    'dashboard.apps.DashboardConfig',
    'levels.apps.LevelsConfig',
    'suscriptions.apps.SuscriptionsConfig',
    'corsheaders',
    'storages',
    'rest_framework',
    'import_export',
    'drf_yasg',
    'raven.contrib.django.raven_compat',
    'django_crontab',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'

AUTHENTICATION_BACKENDS = ['dashboard.helpers.EmailBackend']


# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LOGIN_URL = '/inicio'

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

# DATABASE_ROUTERS = ['payments.routers.PaymentsRouter']

LANGUAGE_CODE = 'es-mx'

TIME_ZONE = 'America/Mexico_City'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# STATICFILES_DIRS = [
#     os.path.join(BASE_DIR, 'static')
# ]

AUTH_USER_MODEL = 'accounts.User'

# GOOGLE CLOUD STORAGE CONFIG(2018)
BLOB_URL = 'https://storage.googleapis.com/storage-towi/'
DEFAULT_FILE_STORAGE = 'storages.backends.gcloud.GoogleCloudStorage'
GS_BUCKET_NAME = 'storage-towi'

# MALING SETTINGS
SENDGRID_API_KEY = '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'
# EMAIL_HOST = 'smtp.zoho.com'
# EMAIL_PORT = 587
# EMAIL_HOST_USER = 'no-reply@towi.com.mx'
# EMAIL_HOST_PASSWORD = 'NyxN3t!!'
# EMAIL_USE_TLS = True
# DEFAULT_FROM_EMAIL = 'no-reply@towi.com.mx'
# EMAIL_USE_SSL = False

# ROUTERS (TO IMPORT OLD DATABASE)
DATABASE_ROUTERS = ['reusable.routers.UsersRouter', ]

# CORS SETTINGS
CORS_ORIGIN_ALLOW_ALL = True

# CELERY SETTINGS
CELERY_BROKER_URL = 'redis://localhost:6379'

# OPENPAY SETTINGS
OPENPAY_API_KEY_SANDBOX = 'sk_6440169060ea49e9a751662104051d1e'
OPENPAY_API_KEY_PUBLIC_SANDBOX = 'pk_f15c186341154b3e8beb84917e5d2b88'
OPENPAY_MERCHANT_ID_SANDBOX = 'muvgiemqbyixlwqth9ok'

# SANDBOX PLANS
OPENPAY_PLAN_1_SANDBOX = 'pqprexrmaqqr5uubvxcm'
OPENPAY_PLAN_2_SANDBOX = 'pconfnhkaavq5kdjdyrt'
OPENPAY_PLAN_3_SANDBOX = 'pwzlqr9skwwfhlvmxvxv'

OPENPAY_API_KEY = 'sk_ab4ceed6d41d49d98f8eba03ed2a73d7'
OPENPAY_MERCHANT_ID = 'm3snrnwej7pwrkeb0zq6'
OPENPAY_API_KEY_PUBLIC = 'pk_35eb40ab57124f32b5ebe71188379afd'

OPENPAY_PLAN_1 = 'p5zsqe0pfdswzrzvdclz'
OPENPAY_PLAN_2 = 'pfylzhcuwdsd1pgmq4rq'
OPENPAY_PLAN_3 = 'pkwoja0xrq5iuuagn8jz'
OPENPAY_PLAN_4 = 'pypflw67hgyjres5pca5'
OPENPAY_PLAN_5 = 'pyhvrq2docqkxnjjkr9y'
OPENPAY_PLAN_6 = 'pkv4c0vctz4mpavuzvkq'
OPENPAY_PLAN_7 = 'pdpebhnlx3jrg1wfcz70'
OPENPAY_PLAN_8 = 'phdk8fivbw1zbsa9vqbe'
OPENPAY_PLAN_9 = 'pay47vqj6ufvtv2wrvxl'
OPENPAY_PLAN_10 = 'psjzcaojm5wnv507yxwf'


CRONJOBS = [
    ('10 1 * * *', 'levels.cron.restart_active_missions', '>> /tmp/cron_active_missions.log')
]